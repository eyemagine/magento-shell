/**
 * EYEMAGINE - The leading Magento Solution Partner
 *
 * Eyemagine Shell Scripting Tools
 *
 * @author    EYEMAGINE <magento@eyemaginetech.com>
 * @category  Eyemagine
 * @package   Eyemagine_Shell
 * @copyright Copyright (c) 2013 EYEMAGINE Technology, LLC (http://www.eyemaginetech.com)
 * @license   http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

-------------------------------------------------------------------------------
DESCRIPTION:
-------------------------------------------------------------------------------

Extends Magento's default abstract shell class.

Module Files:

  - app/etc/modules/Eyemagine_Shell.xml
  - app/code/local/Eyemagine/Shell/*
  - shell/eyemagine/


-------------------------------------------------------------------------------
COMPATIBILITY:
-------------------------------------------------------------------------------

  - Magento Enterprise Edition 1.10.0 to 1.13.1.0
  

-------------------------------------------------------------------------------
RELEASE NOTES:
-------------------------------------------------------------------------------
    
v0.0.3: June 23, 2014
  - Added memory usage
    
v0.0.2: June 13, 2014
  - Significant changes to make outputting easier
  - Logging feature added
    
v0.0.1: September 5, 2013
  - Initial release