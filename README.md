Magento-Shell
=============

Spice up your shell scripts with colors and logging!

See examples in shell/eyemagine/

![Example](http://i.imgur.com/B1276y2.png)
