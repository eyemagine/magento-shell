<?php
/**
 * EYEMAGINE - The leading Magento Solution Partner
 *
 * Eyemagine Shell Scripting Tools
 *
 * @author    EYEMAGINE <magento@eyemaginetech.com>
 * @category  Eyemagine
 * @package   Eyemagine_Shell
 * @copyright Copyright (c) 2013 EYEMAGINE Technology, LLC (http://www.eyemaginetech.com)
 * @license   http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

require_once dirname(__FILE__) . '/../abstract.php';

/**
 * Eyemagine shell script
 *
 * @category    Eyemagine
 * @package     Eyemagine_Shell
 * @author      Eyemagine Technology <magento@eyemaginetech.com>
 */
abstract class Eyemagine_Shell_Abstract extends Mage_Shell_Abstract
{
    protected $_indentChar = " ";
    protected $_indentSize = 4;
    protected $_foregroundColors = array();
    protected $_backgroundColors = array();
    /**
     * Holds to request time start in microtime
     * 
     * @var float
     */
    protected $_startTime;
    /**
     * Enables/disabled logging output
     * 
     * @var bool
     */
    protected $_canOutput = true;
    /**
     * Enables/disabled colorized console
     * 
     * @var bool
     */
    protected $_canUseColor = true;
    /**
     * Output settings
     */
    protected $_debug = false;
    protected $_error = true;
    // in null, no logging
    protected $_logFile;
    /**
     * Captures request time start
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->_startTime = microtime(true);
        $this->_canOutput = !$this->issetArg('quiet');
        $this->_canUseColor = !$this->issetArg('nocolor') && $this->_canOutput;
        if ($this->issetArg('help') && $this->_canOutput) {

            echo $this->usageHelp();
            exit;
        }
        if ($this->_canUseColor) {
            $this->_foregroundColors['black'] = '0;30';
            $this->_foregroundColors['dark_gray'] = '1;30';
            $this->_foregroundColors['blue'] = '0;34';
            $this->_foregroundColors['light_blue'] = '1;34';
            $this->_foregroundColors['green'] = '0;32';
            $this->_foregroundColors['light_green'] = '1;32';
            $this->_foregroundColors['cyan'] = '0;36';
            $this->_foregroundColors['light_cyan'] = '1;36';
            $this->_foregroundColors['red'] = '0;31';
            $this->_foregroundColors['light_red'] = '1;31';
            $this->_foregroundColors['purple'] = '0;35';
            $this->_foregroundColors['light_purple'] = '1;35';
            $this->_foregroundColors['brown'] = '0;33';
            $this->_foregroundColors['yellow'] = '1;33';
            $this->_foregroundColors['light_gray'] = '0;37';
            $this->_foregroundColors['white'] = '1;37';

            $this->_backgroundColors['black'] = '40';
            $this->_backgroundColors['red'] = '41';
            $this->_backgroundColors['green'] = '42';
            $this->_backgroundColors['yellow'] = '43';
            $this->_backgroundColors['blue'] = '44';
            $this->_backgroundColors['magenta'] = '45';
            $this->_backgroundColors['cyan'] = '46';
            $this->_backgroundColors['light_gray'] = '47';
        }
        $this->clearScreen();
    }
    /**
     * Prints messages to console
     *
     * @param string $message
     * @param int $indent
     * @param bool $showTime
     * 
     * @return void
     */
    public function out($message, $indent = 0, $showTime = true)
    {
        if ($this->_canOutput) {
            $time = '';
            if ($showTime && !$this->issetArg('notime')) {
                $time = microtime(true) - $this->_startTime;
                if ($time > 60 * 60) {
                    $time /= 60 * 60;
                    $stamp = 'h';
                }
                else if ($time  > 60) {
                    $time /= 60;
                    $stamp = 'm';
                }
                else {
                    $stamp = 's';
                }
                $time = str_pad((float)round($time, 3) . " " . $stamp, 12);
            }
            $message = str_repeat($this->_indentChar, $indent * $this->_indentSize) . $message;
            echo $time . $message . "\n";
            if (!is_null($this->_logFile)) {
                Mage::log(preg_replace('/\\033.?\[([0-9]+|\;?)+m/', '', $message), null, $this->_logFile);
            }
        }
    }
    /**
     * Clears shell console
     *
     * @return void
     */
    public function clearScreen()
    {
        if (php_sapi_name() == 'cli' && $this->issetArg('clear') && $this->_canOutput) {
            array_map(create_function('$a', 'print chr($a);'), array(27, 91, 72, 27, 91, 50, 74));
        }
    }
    /**
     * Sets the usage help menu
     * 
     * @return array
     */
    public function getUsage()
    {
        return array(
            'commands' => array(
                "help"    => "Shows this help menu",
                "quiet"   => "Disables output",
                "clear"   => "Clears the terminal on run",
                "notime"  => "Removes time since execution from output",
                "nocolor" => "Disables use of colored terminal"
            ),
            'extras' => array()
        );
    }
    /**
     * Retrieve Usage Help Message
     *
     * @return string
     */
    public function usageHelp()
    {
        $usage = $this->getUsage();
        $string = "\nUsage: php " . $_SERVER['SCRIPT_FILENAME'] . " [options]\n";

        $commandsKeys = array_keys($usage['commands']);
        $cmdPad = max(array_map('strlen', $commandsKeys)) + 2;

        ksort($usage['commands']);
        foreach ($usage['commands'] as $command => $desc) {
            $string .= "\n    " . str_pad($command, $cmdPad) . $desc;
        }

        $string .= "\n";

        foreach ($usage['extras'] as $extra) {
            $string .= "\n    " . $extra;
        }

        return $string . "\n\n";
    }
    /**
     * Checks if arguement was supplied
     *
     * @param string $arg     * 
     * @return bool
     */
    public function issetArg($arg)
    {
        return isset($this->_args[$arg]);
    }
    /**
     * Adds colors to terminal output
     * 
     * @param string $string
     * @param string $foreground
     * @param string $background     * 
     * @return string
     */
    public function colorize($string, $foreground = null, $background = null)
    {
        if (!$this->_canUseColor) return $string;
        $coloredString = "";

        // Check if given foreground color found
        if (isset($this->_foregroundColors[$foreground])) {
            $coloredString .= "\033[" . $this->_foregroundColors[$foreground] . "m";
        }
        // Check if given background color found
        if (isset($this->_backgroundColors[$background])) {
            $coloredString .= "\033[" . $this->_backgroundColors[$background] . "m";
        }

        // Add string and end coloring
        $coloredString .=  $string . "\033[0m";

        return $coloredString;
    }
    /**
     * Debug message
     * 
     * @param  string  $string
     * @param  integer $indent
     * @param  boolean $showTime
     * @return void
     */
    public function debug($string, $indent = 0, $showTime = true)
    {
        if ($this->_debug) {
            $this->out($this->colorize($string, 'cyan'), $indent, $showTime);
        }
    }
    /**
     * Success message
     * 
     * @param  string  $string
     * @param  integer $indent
     * @param  boolean $showTime
     * @return void
     */
    public function success($string, $indent = 0, $showTime = true)
    {
        if ($this->_debug) {
            $this->out($this->colorize($string, 'white', 'green'), $indent, $showTime);
        }
    }
    /**
     * Error message
     * 
     * @param  string  $string
     * @param  integer $indent
     * @param  boolean $showTime
     * @return void
     */
    public function error($string, $indent = 0, $showTime = true)
    {
        if ($this->_error) {
            $this->out($this->colorize($string, 'white', 'red'), $indent, $showTime);
        }
    }
    /**
     * Shows progress during iteration
     * 
     * @param  string  $string
     * @param  integer $i
     * @param  integer $count
     * @param  integer $indent
     * @return void
     */
    public function progress($string, $i, $count, $indent = 0)
    {
        if ($this->_debug) {
            $this->out(
                $string . $this->colorize(
                    ' (' . $i . '/' . $count . ' ' . round($i / $count * 100, 2) . '%)',
                    'yellow'
                ), $indent
            );
        }
    }
    /**
     * Shows how much memory has been used
     * 
     * @return string
     */
    public function getMemoryUsage()
    {
        $memoryLimit = ini_get('memory_limit');
        if (preg_match('/^(\d+)(.)$/', $memoryLimit, $matches)) {
            if ($matches[2] == 'M') {
                $memoryLimitBytes = $matches[1] * 1024 * 1024;
            } else if ($matches[2] == 'K') {
                $memoryLimitBytes = $matches[1] * 1024;
            }
        }
        $usedBytes = memory_get_usage();
        $unit = array('', 'K', 'M', 'G');
        $i = floor(log($usedBytes, 1024));
        $used = round($usedBytes / pow(1024, ($i)), 2) . $unit[$i];
        return $used . ' memory used out of ' . $memoryLimit
            . ' (' . round($usedBytes / $memoryLimitBytes * 100, 2) . '%)';
    }
}
