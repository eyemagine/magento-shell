<?php
/**
 * EYEMAGINE - The leading Magento Solution Partner
 *
 * Eyemagine Shell Scripting Tools
 *
 * @author    EYEMAGINE <magento@eyemaginetech.com>
 * @category  Eyemagine
 * @package   Eyemagine_Shell
 * @copyright Copyright (c) 2013 EYEMAGINE Technology, LLC (http://www.eyemaginetech.com)
 * @license   http://opensource.org/licenses/afl-3.0.php Academic Free License (AFL 3.0)
 */

require_once 'abstract.php';

/**
 * Eyemagine shell script
 *
 * @category    Eyemagine
 * @package     Eyemagine_Shell
 * @author      Eyemagine Technology <magento@eyemaginetech.com>
 */

class Eyemagine_Shell extends Eyemagine_Shell_Abstract
{
    /**
     * Output settings
     */
    protected $_debug = true;
    protected $_error = true;
    // in null, no logging
    protected $_logFile = 'shell_hidechildproducts.log';
    /**
     * Runs scripts
     * @return void
     */
    public function run()
    {
        if (!$this->getArg('run')) {
            echo $this->usageHelp();
            exit;
        }
        Mage::app()->getStore()->setId(Mage_Core_Model_App::ADMIN_STORE_ID);
        $confResource = Mage::getResourceSingleton('catalog/product_type_configurable');
        $configurables = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToFilter('type_id', array('eq' => Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE));
        $this->debug('Found ' . $configurables->getSize() . ' configurable products');
        $children = array();
        foreach ($configurables as $configurable) {
            $confChildren = $confResource->getChildrenIds($configurable->getId(), true);
            $children = array_merge($children, array_values($confChildren[0]));
        }
        $this->debug('Found ' . count($children) . ' children products to update');
        if (count($children)) {
            $this->_runAndTellThat($children);
        }
        else {
            $this->debug('No products to update');
        }
    }
    /**
     * Hide yo kids, hide you wife
     * 
     * @param  array
     * @return void
     */
    protected function _runAndTellThat($ids)
    {
        $this->debug('Updating visibility...');
        try {
            Mage::getSingleton('catalog/product_action')->updateAttributes(
                $ids,
                array('visibility' => Mage_Catalog_Model_Product_Visibility::VISIBILITY_NOT_VISIBLE),
                Mage_Core_Model_App::ADMIN_STORE_ID
            );
            $this->success('Simple products were updated successfully');
        }
        catch(Exception $e) {
            $this->error("Failed to update visibilty\n" . $e->getMessage());
        }
    }
    /**
     * Shows usage
     * 
     * @return array
     */
    public function getUsage()
    {
        return array_merge_recursive(parent::getUsage(), array('commands' => array(
            'run' => 'Runs script'
        ), 'extras' => array(
            'Hides all children products that belong to a parent configurable'
        )));
    }

}

$shell = new Eyemagine_Shell();
$shell->run();