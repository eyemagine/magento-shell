<?php

require_once 'abstract.php';

/**
 * Eyemagine shell script
 *
 * @category    Eyemagine
 * @package     Eyemagine_Shell
 * @author      Eyemagine Technology <magento@eyemaginetech.com>
 */
class Eyemagine_Shell extends Eyemagine_Shell_Abstract
{
    /**
     * Output settings
     */
    protected $_debug = true;
    protected $_error = true;
    // in null, no logging
    protected $_logFile = 'shell_stripProductHtml.log';
    public function run()
    {
        $this->debug('Stripping HTML from all short descriptions...');

        $products = Mage::getResourceModel('catalog/product_collection')
            ->addAttributeToSelect('short_description');

        $count = $products->count();

        $this->debug('Found ' . $count . ' product(s)');

        $i = 0;
        foreach ($products as $product) {
            $this->progress('SKU ' . $product->getSku(), ++$i, $count, 1);
            $old = $product->getShortDescription();
            $this->debug('Old: ' . $this->colorize($old, 'white', 'magenta'), 2);
            $new = strip_tags($old);
            if (empty($new)) {
                $this->error('New short description was empty after cleaning.  Skipping...', 2);
                continue;
            }
            $this->debug('New: ' . $this->colorize($new, 'white', 'magenta'), 2);
            if ($new == $old) {
                $this->debug('Short descriptions do not differ. Nothing to do.  Skipping...', 2);
                continue;
            }
            $product->setShortDescription($new);
            try {
                $product->save();
                $this->success('Updated product', 2);
            }
            catch (Exception $e) {
                $this->error("Failed to update product\n" . $e->getMessage(), 2);
            }
        }

        $this->debug('Script complete.');
    }
}

$shell = new Eyemagine_Shell();
$shell->run();
